import React from 'react';
import '../css/nav.css';
import { Link } from 'react-router-dom';
import { useState } from 'react';

export const Nav = (props) => {
    const [searchText, setSearchText] = useState("");

    const handleUserInput = (e) => {
        setSearchText(e.target.value);
    };

    const submitSearchText = () => {
        setSearchText("");
        props.onSearchText(searchText);
    };

    return (
        <div className="nav">
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                <div className="container-fluid">
                    <Link className="navbar-brand" to='/'>The Guitar Place</Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-1 mb-lg-0">
                            <li className="nav-item">
                                <Link className="nav-link active" to='/customshop'>Custom</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link active" to='/account'>Account</Link>
                            </li>
                            <li className="nav-item nav-link active">
                                <span className="nav__cart" onClick={props.onShowCart}>Cart</span>
                            </li>
                        </ul>
                        <input className="form-control" type="text" value={searchText} placeholder='Search Guitar Place...' onChange={handleUserInput} />
                         &nbsp;
                        <button className="btn btn-secondary" onClick={submitSearchText}>Submit</button>
                    </div>
                </div>
            </nav>
        </div>
    );
};
