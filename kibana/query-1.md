# 
GET products/guitar/_search
{
  "query" : { 
    "match" : {
      "model" : "suhr modern" 
    } 
  }
}

# BROAD
GET products/guitar/_search
{
  "query": {
    "query_string": {
      "query": "brown suhr"
    }
  }
}

# Match phrase
GET products/guitar/_search
{
  "query" : { 
    "match_phrase": {
      "model" : "suhr modern" 
    } 
  }
}

# Wildcard
GET products/guitar/_search
{
  "query" : { 
    "wildcard": {
      "model" : {
        "value" : "s*r"
      }
    } 
  }
}

# MultiMatch
GET products/guitar/_search
{
  "query" : { 
    "multi_match": {
      "query" : "suhr",
      "fields": [ "model", "color"]
      }
    } 
}


# List all guitars
GET products/guitar/_search
{
  "query": {
    "match_all": {}
  }
}

# DELETE
DELETE products/guitar/MFF2tX4Bpju_5k0n36Sy

# ADD
POST products/guitar
{
    "id" : 3,
    "model" : "suhr modern",
    "color" : "black",
    "neck": "10"
}


# Broad
GET products/guitar/_search
{
  "query": {
    "query_string": {
      "query": "brown suhr"
    }
  }
}

# Template
GET products/guitar/_search
{
  "_source": [],
  "size": 10,
  "query": {
    "bool": {
      "must": [],
      "filter": [],
      "should": [],
      "must_not": []
    }
  }
}

# 
GET products/guitar/_search
{
  "_source": [],
  "size": 10,
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "model": "suhr"
          }
        },
        {
          "match": {
            "color": "brown"
          }
        }
      ],
      "filter": [],
      "should": [],
      "must_not": []
    }
  }
}

