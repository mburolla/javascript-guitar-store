# Elasticsearch

- [Download ES](https://www.elastic.co/downloads/elasticsearch)
- Execute: `bin/elasticsearch.bat`
- URL: `http://localhost:9200/products/guitar/`

-[Kibana](https://www.elastic.co/guide/en/kibana/current/windows.html)
- Execute: `bin/kibana.bat`
- URL: `http://localhost:5601/app/dev_tools#/console`
- Left menu: DevTools

http://localhost:9200/<INDEX>/<TYPE>
- Index ==> DB
- Type ==> Table
- Record ==> Rows

```
{
  "_source": [],
  "size": 10,
  "min_score" : 1,
  "query":
    { 
        "bool": {
            "must" : [],   <=== AND
            "filter" : [],
            "should" : [], <=== OR
            "must_not" : []
        }
    }
}
```

# Snippets

```
{
  "_source": [],
  "size": 10,
  "min_score" : 1,
  "query":
    { 
        "bool": {
            "must" : [],
            "filter" : [],
            "should" : [],
            "must_not" : []
        }
    }
}
```