# Guitar Place
The world's first Elasticsearch [guitar store](http://js-react-2.s3-website-us-east-1.amazonaws.com/) built using React and AWS OpenSearch (Elasticsearch).

![](./docs/guitar-store.png)

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Run the app: `npm start`
- Build & Deploy: `./devops/build-deploy-bat`

# React Architecture
![](./docs/gs_arch.png)

# Overall Architecture
TODO

# S3 Bucket Configuration
- Enable website hosting
- Add index.html to properties
- Add the following bucket policy:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::<BUCKET NAME>/*"
        },
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::<AWS ACCOUNT NUMBER NO DASHES>:user/<AWS USERNAME>"
            },
            "Action": "*",
            "Resource": "arn:aws:s3:::<BUCKET NAME>/*"
        },
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::<AWS ACCOUNT NUMBER NO DASHES>:user/<AWS USERNAME>"
            },
            "Action": "*",
            "Resource": "arn:aws:s3:::<BUCKET NAME>"
        }
    ]
}
```

# Notes
- [React Router](https://reactrouter.com/docs/en/v6/getting-started/tutorial)
- [React Router Video](https://youtu.be/Law7wfdg_ls)
- [UseEffect](https://stackoverflow.com/questions/53219113/where-can-i-make-api-call-with-hooks-in-react)
- [UseEffect Video](https://www.youtube.com/watch?v=k0WnY0Hqe5c)
- [Dynamically Loading](https://www.upbeatcode.com/react/where-to-store-images-in-react-app/)

# Elasticsearch
[Intro](https://youtu.be/dPCFGt1AuJw)
